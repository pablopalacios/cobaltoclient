class Class:

    NULL_STATUS = (
        'dispensado',
        'trancamento de disciplina',
        'trancado',
        'mobilidade acadêmica'
    )

    def __init__(self, data):
        self.name = data['disciplina'].strip()
        self.status = self._set_status(data['situacao'])
        self.grade = self._set_grade(data['nota_final'])
        self.year = int(data['ano'])
        # self.semester = int(data['periodo'][0])

    def _set_grade(self, grade):
        if grade:
            return float(grade)
        else:
            return 0

    def _set_status(self, status):
        if status.strip().lower() in Class.NULL_STATUS:
            return False
        else:
            return True
