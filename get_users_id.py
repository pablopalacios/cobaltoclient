import os
import threading

from optparse import OptionParser
from time import time, sleep

from client import CobaltoClient


BASE_DIR = 'users'


class Request(threading.Thread):

    def __init__(self, user_id, curso_id, *args, **kw):
        threading.Thread.__init__(self, *args, **kw)
        self.user_id = user_id
        self.course_id = curso_id
        self.error_file_name = os.path.join(BASE_DIR, 'errors', self.user_id)
        self.file_name = os.path.join(BASE_DIR, 'ids', self.user_id)

    def is_from_course(self):
        grades = client.get_grades(self.user_id, self.course_id)
        if 'rows' in grades:
            return True

    def run(self):
        try:
            if self.is_from_course():
                with open(self.file_name, 'w'):
                    pass
        except Exception as e:
            with open(self.error_file_name, 'w') as fp:
                fp.write('ERRO: {}'.format(e))


def get_ids(curso_id, start, stop, max_threads):
    bgthreads = threading.active_count()
    threads = (Request(str(user_id), curso_id) for user_id in range(start, stop))
    for thread in threads:
        thread.start()
        while threading.active_count() > (bgthreads + max_threads):
            sleep(.2)


def main():
    parser = OptionParser()
    parser.add_option('-b', '--begin', action='store',
                      dest='begin', default='0', help='First ID to search')
    parser.add_option('-n', '--number', action='store', dest='number',
                      default='1000', help='Number of ids to search')
    parser.add_option('-m', '--max-threads', action='store', dest='max_threads',
                      default='500', help='Maximum number of threads to start')

    options, args = parser.parse_args()
    begin = int(options.begin)
    end = begin + int(options.number)
    max_threads = int(options.max_threads)

    start = time()
    get_ids('172', begin, end, max_threads)
    stop = time()
    print(max_threads, "threads:", stop-start)
            

if __name__ == '__main__':
    client = CobaltoClient('41490505890', '123mudarUfpel!')
    client.login()
    main()
