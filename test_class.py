import json
import unittest

from class_ import Class


class TestClass(unittest.TestCase):

    def test_Class(self):
        data = json.loads("""{"id": "130722",
             "nome": "PABLO PAL\u00c1CIOS",
             "tipo": "DISCIPLINA",
             "situacao": "Aprovado",
             "cod_matricula": "12101841",
             "cod_atividade": "0460484",
             "disciplina": "MUSICOLOGIA",
             "ano": "2015",
             "periodo": "1\u00ba Semestre",
             "ano_periodo": "2015\/1",
             "creditos": "2",
             "ch": null,
             "cod_turma": "M1",
             "nota_final": "10.00",
             "conceito_final": null,
             "nr_faltas": "2.0"
        }""")
        class_ = Class(data)
        self.assertEqual(class_.year, 2015)
        self.assertEqual(class_.semester, 1)
        self.assertEqual(class_.name, 'MUSICOLOGIA')
        self.assertEqual(class_.grade, 10)

if __name__ == '__main__':
    unittest.main()
