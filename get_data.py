import os

from time import sleep

from client import CobaltoClient


def get_data(id_list, course_id):
    client = CobaltoClient('41490505890', '123mudarUfpel!')
    client.login()

    print('Retrieving data...')

    size = len(id_list)
    for i, id_ in enumerate(id_list):
        print('\r', end='')
        print('  Getting {} of {}'.format(i+1, size), end='')
        client.write(id_, course_id)
    print('\nCompleted!')
