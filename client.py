import json

from http.cookiejar import CookieJar
from urllib.parse import urlencode
from urllib.request import build_opener, HTTPCookieProcessor


BASE_URL = 'https://cobalto.ufpel.edu.br/'


class CobaltoClient:

    def __init__(self, user, password):
        self.user = user
        self.password = password

        self.cookies = CookieJar()
        self.driver = build_opener(HTTPCookieProcessor(self.cookies))

    @staticmethod
    def response_to_json(response):
        page = response.read()
        json_ = json.loads(page.decode())
        return json_

    def _post(self, url, data):
        url = BASE_URL + url
        data = urlencode(data).encode()
        response = self.driver.open(url, data)
        return response

    def _get(self, url, query):
        query = urlencode(query).replace('%2B', '+').replace('%2C', ',')
        url = BASE_URL + url + '/?' + query
        response = self.driver.open(url)
        return response

    def login(self):
        url = 'autenticacao/login/entrar'
        data = {
            'txtEmail': self.user,
            'txtSenha': self.password,
        }
        response = CobaltoClient.response_to_json(self._post(url, data))
        return response

    def get_grades(self, user_id, course_id):
        url = 'academico/alunos/historicoAluno/listaHistoricoAluno'
        query = {
            'null': '',
            'curso_versao_id': course_id,
            'pessoaId': user_id,
            '_search': 'false',
            'nd': '1438115882204',
            'rows': '-1',
            'page': '1',
            'sidx': 'ano_periodo+desc,+ano',
            'sord': 'desc'
        }
        response = CobaltoClient.response_to_json(self._get(url, query))
        return response

    def get_enrollment_requests(self, user_id, course_id):
        url = 'academico/alunos/solicitacaoMatricula/buscaSolicitacoes'
        data = {
            'pessoa_id': user_id,
            'curso_versao_id': course_id,
        }
        response = CobaltoClient.response_to_json(self._post(url, data))
        return response

    def get_available_classes(self, user_id, course_id):
        url = 'academico/alunos/solicitacaoMatricula/listaCursosVersoesDisciplinas'
        query = {
            'null': '',
            'curso_versao_id': course_id,
            'pessoa_id': user_id,
            '_search': 'true',
            'nd': '1438118768341',
            'rows': '-1',
            'page': '1',
            'sidx': 'nr_periodo+asc,+nome+asc,+nome',
            'sord': 'asc',
        }
        response = CobaltoClient.response_to_json(self._get(url, query))
        return response

    @staticmethod
    def get_name_from_grades(grades):
        return grades['rows'][0]['nome']

    @staticmethod
    def get_registry_from_grades(grades):
        return grades['rows'][0]['cod_matricula']

    def as_dict(self, user_id, course_id):
        grades = self.get_grades(user_id, course_id)
        requests = self.get_enrollment_requests(user_id, course_id)
        classes = self.get_available_classes(user_id, course_id)
        name = CobaltoClient.get_name_from_grades(grades)
        registry = CobaltoClient.get_registry_from_grades(grades)

        d = {
            'id': user_id,
            'course_id': course_id,
            'name': name,
            'registry': registry,
            'grades': grades,
            'available_requests': classes,
            'requests': requests
        }
        return d

    def write(self, user_id, course_id):
        fn = 'users/json/{}.json'.format(user_id)
        with open(fn, 'w') as fp:
            json.dump(self.as_dict(user_id, course_id), fp)
