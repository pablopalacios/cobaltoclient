import json
import unittest

from client import CobaltoClient


class TestClient(unittest.TestCase):

    def setUp(self):
        self.user = '41490505890'
        self.password = '123mudarUfpel!'

        self.user_id = '66214'
        self.course = '162'

        self.client = CobaltoClient(self.user, self.password)
        self.login_response = self.client.login()

    def test_login_success(self):
        self.assertIn('success', self.login_response)

    def test_login_failed(self):
        client = CobaltoClient('wrong_user', 'wrong_password')
        response = client.login()
        self.assertIn('error', response)

    def test_get_grades(self):
        grades = self.client.get_grades(self.user_id, self.course)
        self.assertGreater(len(grades['rows']), 0)

    def test_get_enrollment_requests(self):
        requests = self.client.get_enrollment_requests(self.user_id, self.course)
        self.assertIn('solicitacoes', requests)

    def test_get_available_classes(self):
        classes = self.client.get_available_classes(self.user_id, self.course)
        self.assertGreater(len(classes['rows']), 0)

    def test_name_and_registry(self):
        grades = self.client.get_grades(self.user_id, self.course)
        self.assertEqual(
            CobaltoClient.get_name_from_grades(grades),
            'PABLO PALÁCIOS'
        )
        self.assertEqual(
            CobaltoClient.get_registry_from_grades(grades),
            '12101841'
        )

    def test_as_dict(self):
        d = self.client.as_dict(self.user_id, self.course)
        self.assertEqual(d['name'], 'PABLO PALÁCIOS')
        self.assertEqual(d['registry'], '12101841')
        self.assertEqual(d['id'], self.user_id)
        self.assertEqual(d['course_id'], self.course)
        self.assertIn('rows', d['grades'])
        self.assertIn('rows', d['available_requests'])
        self.assertIn('solicitacoes', d['requests'])

    def test_write(self):
        self.client.write(self.user_id, self.course)
        with open('users/json/66214.json') as fp:
            d = json.load(fp)
        self.assertEqual(d['name'], 'PABLO PALÁCIOS')
        self.assertEqual(d['registry'], '12101841')
        self.assertEqual(d['id'], self.user_id)
        self.assertEqual(d['course_id'], self.course)
        self.assertIn('rows', d['grades'])
        self.assertIn('rows', d['available_requests'])
        self.assertIn('solicitacoes', d['requests'])


if __name__ == '__main__':
    unittest.main()
