import json

from class_ import Class


class Student:

    def __init__(self, fn):
        with open(fn) as fp:
            self.data = json.load(fp)
        self.id = self.data['id']
        self.name = self.data['name']
        self.registry = self.data['registry']

    def classes(self):
        return tuple(Class(data) for data in self.data['grades']['rows'])

    def grades_average(self):
        grades = tuple(c.grade for c in self.classes() if c.status)
        return round(sum(grades)/len(grades), 1)

    def available_requests(self):
        available_requests = self.data['available_requests']['rows']
        ars = {}
        for ar_data in available_requests:
            ar = AvailableRequest(ar_data)
            ars[ar.id] = ar
        return ars

    def requests(self):
        ars = self.available_requests()
        requests = self.data['requests']['solicitacoes']
        return tuple(str(ars[request['turma_id']]) for request in requests)

    def __str__(self):
        return '{} {} {}'.format(self.registry, self.grades_average(), self.name)


class AvailableRequest:

    def __init__(self, data):
        self.id = data['id']
        self.name = data['nome'].split(' - ')[1]
        self.code = data['cod_turma']
        self.max = int(data['solicitacoes_vagas'].split(' / ')[1])

    def __str__(self):
        return '{} - {} ({} MAX)'.format(self.code, self.name, self.max)
