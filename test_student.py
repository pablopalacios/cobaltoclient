import unittest

from student import Student, AvailableRequest


class TestStudent(unittest.TestCase):

    def setUp(self):
        self.fn = 'users/fixture.json'
        self.student = Student(self.fn)

    def test_student(self):
        self.assertEqual(self.student.id, '10463')
        self.assertEqual(self.student.name, 'JORDANA SANGALLI LUFT')
        self.assertEqual(self.student.registry, '13201009')

    def test_classes(self):
        classes = self.student.classes()
        self.assertEqual(len(classes), 33)

    def test_grades_avarage(self):
        self.assertEqual(self.student.grades_average(), 5.2)

    def test_available_requests(self):
        ars = self.student.available_requests()
        self.assertEqual(len(ars), 43)
        self.assertIn('135141', ars)

    def test_requests(self):
        requests = self.student.requests()
        self.assertEqual(len(requests), 6)
        self.assertIn('M1 - CIRCUITOS DIGITAIS I', requests)


class TestAvailableRequest(unittest.TestCase):

    def setUp(self):
        self.data = {'id': '135141',
                'estrutura': 'DISCIPLINA OBRIGAT\u00d3RIA ',
                'cod_atividade': '1110064',
                'nome': '1110064 - CIRCUITOS DIGITAIS I',
                'creditos': '4',
                'cod_turma': 'M1',
                'solicitacoes_vagas': '35 \/ 20',
                'horarios': 'QUA (10: 00-10: 50)  QUA (10: 50-11: 40)  SEX (13: 30-14: 20)  SEX (14: 20-15: 10)',
                'cod_horarios': '414;621;622;413',
                'nr_periodo': '2\u00ba Semestre',
                'periodo': '2'
        }

    def test_available_request(self):
        ar = AvailableRequest(self.data)
        self.assertEqual(ar.code, self.data['cod_turma'])
        self.assertEqual(ar.name, 'CIRCUITOS DIGITAIS I')
        self.assertEqual(ar.id, '135141')

    def test_str(self):
        ar = AvailableRequest(self.data)
        self.assertEqual(str(ar), 'M1 - CIRCUITOS DIGITAIS I')


if __name__ == '__main__':
    unittest.main()
