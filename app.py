import os
import optparse

from student import Student
from get_data import get_data


class Classification:

    def __init__(self, path):
        os.chdir(path)
        fns = os.listdir()
        self.students = (Student(fn) for fn in fns)

    def classes(self):
        classes = {}
        for student in self.students:
            for request in student.requests():
                classes[request] = classes.get(request, []) + [student]
        return classes

    def print(self):
        for class_, students in self.classes().items():
            print('## {}:'.format(class_))
            classification = enumerate(
                sorted(
                    students,
                    key=lambda student: student.grades_average(),
                    reverse=True
                )
            )
            for position, student in classification:
                print('{:2}º {}'.format(position+1, student))
            print()


def main():
    parser = optparse.OptionParser()
    parser.add_option('-r', '--refresh', action='store_true', dest='refresh')
    options, args = parser.parse_args()

    if options.refresh:
        id_list = os.listdir('users/ids/')
        get_data(id_list, '172')

    Classification('./users/json/').print()


if __name__ == '__main__':
    main()
